var port = process.env.PORT || 8080;
var ip   = process.env.IP   || '127.0.0.1';

var util = require('util');
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
	
// db setup
var sql = require('sql.js');
var db = new sql.Database();
var sqlstr = "CREATE TABLE log (id INTEGER PRIMARY KEY, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, message text);";
db.run(sqlstr);

server.listen(port, ip, function () {
  log(util.format('Server running on http://%s:%s', ip, port));
});

// Routing
app.use(express.static(__dirname + '/public'));

app.get('/log', function (req, res) {

	var html = "";
	html += "<style> #t { font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%; } #t td, #t th { border: 1px solid #ddd; padding: 8px; } #t tr:nth-child(even){background-color: #f2f2f2;} #t tr:hover {background-color: #ddd;} #t th { padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; color: white; } </style>";
	html += "<table id=\"t\">";
	html += "  <tr>";
	html += "    <th>Log Id</th>";
	html += "    <th>Time</th>";
	html += "    <th>Message</th>";
	html += "  </tr>";
	
	db.each("SELECT id, time, message FROM (SELECT id, time, message FROM log ORDER BY id DESC LIMIT 1000) ORDER BY id ASC;", {}, function(row) {
		html += util.format("<tr><td>%s</td><td>%s</td><td>%s</td></tr>", row.id, row.time, row.message);
	});
	
	html += "</table>";
	res.send(html);
});

// Chatroom

// usernames which are currently connected to the chat
var usernames = {};
var numUsers = 0;

io.on('connection', function (socket) {
  var addedUser = false;
  var username = "";

  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
	
	var str = util.format("new message from user: %s: %s", socket.username, data);
	log(str);
	
    // we tell the client to execute 'new message'
    socket.broadcast.emit('new message', {
      username: socket.username,
      message: data
    });
	
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', function (username) {
    
	var str = util.format("add user: %s", socket.username);
	log(str);
	
	// we store the username in the socket session for this client
    socket.username = username;
    // add the client's username to the global list
    usernames[username] = username;
    ++numUsers;
    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers
    });

    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    });
	
	socket.emit('me', 'me');
    socket.broadcast.emit('others', 'others');
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
	  
	var str = util.format("disconnecting user: %s", socket.username);
	log(str);
	  
    // remove the username from global usernames list
    if (addedUser) {
      delete usernames[socket.username];
      --numUsers;

      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers: numUsers
      });

    }
  });
});

function log(message) {
	console.log(message);
	var sqlstr = "INSERT INTO log (id, message) VALUES (NULL, '" + message + "');";
	db.run(sqlstr);
}